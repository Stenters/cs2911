"""
- CS2911 - 022
- Fall 2017
- Lab 5
- Names:
  - Stuart Enters
  - Jake Evenson

A class for formatting HTTP requests
"""


def send_request(host, resource, tcp_socket):
    """
    Helper method for sending a HTTP request
    :param host: the host to request from
    :param resource: the resource to request from
    :param tcp_socket: the socket to send the request through
    :author: Stuart
    """
    # Format the HTTP request
    request_line = make_request_line(resource)
    request_headers = make_header_lines(host)
    request_body = make_request_body()
    request_msg = request_line + request_headers + b'\r\n' + request_body

    # Request the resource
    tcp_socket.sendall(request_msg)


def make_request_line(resource):
    """
    Helper method for formatting the request line for an HTTP request
    :param resource: the resource to being requested from the server
    :return: the formatted request line
    :author: Stuart
    """
    line = ""
    line += "GET " + str(resource, "ASCII") + " HTTP/1.1\r\n"
    return line.encode()


def make_header_lines(host):
    """
    Helper method for formatting the header line(s) for an HTTP request
    :param host: the host to request the resource from
    :return: the formatted header line(s)
    :author: Stuart
    """
    line = ""
    line += "Host: " + str(host, "ASCII") + "\r\n\r\n"
    return line.encode()


def make_request_body():
    """
    Helper method for formatting the body of an HTTP request
    :return: the formatted body
    :author: Stuart
    """
    line = ""
    return line.encode()
