"""
- CS2911 - 022
- Fall 2017
- Lab 5
- Names:
  - Stuart Enters
  - Jake Evenson

A class for reading the response from a HTTP server
"""


def receive_request(tcp_socket, file_name):
    """
    Helper method for receiving a HTTP response
    :param tcp_socket: the socket to receive the request from
    :param file_name: name of file to write to
    :return: the status code
    :author: Jake
    """
    # Receive the response
    response = receive(tcp_socket)
    code = response[0]
    code = int(code)
    write_to_file(response[1], file_name)
    return code


def receive(tcp_socket):
    """
    Helper method for determining what type of response (Content-Length vs chuncked)
    is received and formatting the message
    :param tcp_socket: the socket to receive the response from
    :return: status code, the response from the server
    :author: Jake
    """
    response = b''
    headers = dict()

    # Get the headers
    while not response.endswith(b'\r\n\r\n'):
        response += tcp_socket.recv(1)
    response = response.split(b'\r\n')

    # Get the status code of the response
    status_code = response[0]
    status_code = status_code.split(b' ')
    status_code = status_code[1]
    status_code = int(status_code)

    # Add all the headers to a dictionary
    for line_number in range(1, len(response) - 2):
        temp = response[line_number].split(b': ')
        headers[temp[0]] = temp[1]

    # Check if Transfer Encoding is chunked or content length
    if b'Transfer-Encoding' in headers:
        return status_code, parse_chunked(tcp_socket)

    elif b'Content-Length' in headers:
        size = headers[b'Content-Length']
        size = int(size)
        return status_code, parse_content_length(size, tcp_socket)

    else:
        print("Error with response")
        return status_code, ""


def write_to_file(response, file_name):
    """
    Method for writing resources to a file
    :param response: the resource to write to file as a bytes object (b'')
    :param file_name: the name of the file to write to
    :author: Jake
    """
    with open(file_name, 'wb') as output_file:
        output_file.write(response)


def parse_content_length(size, tcp_socket):
    body = b''
    for a in range(size):
        body += tcp_socket.recv(1)
    return body


def parse_chunked(tcp_socket):
    """
    Helper method for reading the body of a chunked HTTP response
    :param tcp_socket: the socket containing the response body
    :return: the body of the message as a bytes object (b'')
    :author: Jake
    """
    size = tcp_socket.recv(1)
    size = get_size(size, tcp_socket)
    read = int(size, 16)
    message = b''
    while not size == 0:

        buffer = tcp_socket.recv(2)
        message += tcp_socket.recv(read)
        buffer = tcp_socket.recv(2)
        if message.endswith(b'\r\n\r\n'):
            return message
        size = tcp_socket.recv(1)
        size = get_size(size, tcp_socket)
    return message


def get_size(size, tcp_socket):
    while not size.endswith(b'\r\n'):
        size += tcp_socket.recv(1)
    size.split(b'\r\n')
    return size[0]
