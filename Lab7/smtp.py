"""
- CS2911 - 0NN
- Fall 2017
- Lab N
- Names:
  - Stuart Enters
  - Jake Evenson

A simple email sending program.

Thanks to Trip Horbinski from the Fall 2015 class for providing the password-entering functionality.
"""

# GUI library for password entry
import tkinter as tk

# Socket library
import socket

# SSL/TLS library
import ssl

# base-64 encode/decode
import base64

# Python date/time and timezone modules
import datetime
import pytz
import tzlocal

# Module for reading password from console without echoing it
# import getpass

# Modules for some file operations
import os
import mimetypes

# Host name for MSOE (hosted) SMTP server
SMTP_SERVER = 'smtp.office365.com'

# The default port for STARTTLS SMTP servers is 587
SMTP_PORT = 587

# SMTP domain name
SMTP_DOMAINNAME = 'msoe.edu'

# Msg info
RECPT = "enterss@msoe.edu"
SUBJECT = "Here's my lab"
BODY = "Sent via smtp.py\r\n\r\nI need more ☕"
ATTACH = "sans.jpg"

PASS_FILE = "_pass.txt"


def main():
    """Main test method to send an SMTP email message.

    Modify data as needed/desired to test your code,
    but keep the same interface for the smtp_send
    method.
    """
    (username, password) = login_gui()

    message_info = dict()
    message_info['To'] = RECPT
    message_info['From'] = username
    message_info['Subject'] = SUBJECT
    message_info['Date'] = get_formatted_date()

    print("message_info =", message_info)

    message_text = BODY

    smtp_send(username, password, message_info, message_text)


def login_gui():
    """
    Creates a graphical user interface for secure user authorization.

    :return: (email_value, password_value)
        email_value -- The email address as a string.
        password_value -- The password as a string.

    :author: Tripp Horbinski
    """
    gui = tk.Tk()
    gui.title("MSOE Email Client")
    center_gui_on_screen(gui, 370, 120)

    tk.Label(gui, text="Please enter your MSOE credentials below:") \
        .grid(row=0, columnspan=2)
    tk.Label(gui, text="Email Address: ").grid(row=1)
    tk.Label(gui, text="Password:         ").grid(row=2)

    email = tk.StringVar()
    email_input = tk.Entry(gui, textvariable=email)
    email_input.grid(row=1, column=1)

    password = tk.StringVar()
    password_input = tk.Entry(gui, textvariable=password, show='*')
    password_input.grid(row=2, column=1)

    auth_button = tk.Button(gui, text="Authenticate", width=25, command=gui.destroy)
    auth_button.grid(row=3, column=1)

    gui.mainloop()

    email_value = email.get()
    password_value = password.get()

    return email_value, password_value


def center_gui_on_screen(gui, gui_width, gui_height):
    """Centers the graphical user interface on the screen.

    :param gui: The graphical user interface to be centered.
    :param gui_width: The width of the graphical user interface.
    :param gui_height: The height of the graphical user interface.
    :return: The graphical user interface coordinates for the center of the screen.
    :author: Tripp Horbinski
    """
    screen_width = gui.winfo_screenwidth()
    screen_height = gui.winfo_screenheight()
    x_coord = (screen_width / 2) - (gui_width / 2)
    y_coord = (screen_height / 2) - (gui_height / 2)

    return gui.geometry('%dx%d+%d+%d' % (gui_width, gui_height, x_coord, y_coord))

# *** Do not modify code above this line ***


def smtp_send(username, password, message_info, message_text):
    """Send a message via SMTP.

    :param username: The username of the account sending the email
    :param password: String containing user password.
    :param message_info: Dictionary with string values for the following keys:
                'To': Recipient address (only one recipient required)
                'From': Sender address
                'Date': Date string for current date/time in SMTP format
                'Subject': Email subject
            Other keys can be added to support other email headers, etc.
    :param message_text: the text of the message to send
    """

    # Create the connection to the server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((SMTP_SERVER, SMTP_PORT))
    check_status_code(read(sock), b'220')

    # Upgrade to TLS
    encrypted_sock = start_tls_connection(sock)

    # Send the message
    print_msg_to_server(encrypted_sock, username, password, message_info, message_text)


def start_tls_connection(tcp_socket):
    """
    Helper method for performing handshake with server and upgrading to secure TLS connection
    :param tcp_socket: the socket over which to communicate to the server
    :return: the upgraded TCP socket
    :author: Stuart Enters
    """
    # Say Hi to the server
    response = send_msg("EHLO " + SMTP_DOMAINNAME, tcp_socket)
    check_status_code(response[0].split(b'-')[0], b'250')

    # Upgrade to secure connection
    response = send_msg("STARTTLS", tcp_socket)
    check_status_code(response, b'220')

    # Wrap the socket
    context = ssl.create_default_context()
    return context.wrap_socket(tcp_socket, server_hostname=SMTP_SERVER)


def print_msg_to_server(tcp_socket, username, password, message_info, message_text):
    """
    Method for communicating with a SMTP server over a TLS secure connection to send an email
    :param tcp_socket: the socket over which to communicate to the server
    :param username: The username of the account sending the email
    :param password: the password of the account sending the email
    :param message_info: the meta data of the message being sent
    :param message_text: the text of the message to be sent
    :return: None
    :author: Stuart Enters
    """
    # Say Hi
    response = send_msg("EHLO " + SMTP_DOMAINNAME, tcp_socket)
    check_status_code(response[0].split(b'-')[0], b'250')

    # Login
    response = send_msg("AUTH LOGIN", tcp_socket)
    check_status_code(response, b'334')
    login(username, password, tcp_socket)

    # Send parameters
    send_message_params(message_info, tcp_socket)

    # Send the message
    send_message(message_info, message_text, tcp_socket)

    # Close the connection
    response = send_msg("Quit", tcp_socket)
    check_status_code(response, b'221')
    tcp_socket.close()


def is_tls(response):
    """
    Helper method for verifying if the server handles TLS
    :param response: the response from the server to the initial EHLO command
    :return: if the server supports TLS
    :author: Stuart Enters
    """
    if response[len(response) - 1].split(b' ')[1] == b"STARTTLS":
        return True
    for line_num in range(0, len(response) - 2):
        if response[line_num].split(b'-')[1] == b"STARTTLS":
            return True
    return False


def login(username, password, tcp_socket):
    """
    Helper method for logging into a SMTP server
    :param username: The username of the account sending the email
    :param password: The password of the account sending the email
    :param tcp_socket: the socket over which to communicate to the server 
    :return: None
    :author: Stuart Enters
    """
    username = (base64.b64encode(username.encode("utf-8"))).decode("utf-8")
    password = (base64.b64encode(password.encode("utf-8"))).decode("utf-8")

    response = send_msg(username, tcp_socket)
    check_status_code(response, b'334')

    response = send_msg(password, tcp_socket)
    check_status_code(response, b'235')


def send_message_params(message_info, tcp_socket):
    """
    Helper method for specifying sender and receiver of an email
    :param message_info: meta data about the message being sent
    :param tcp_socket: the socket over which to communicate to the server 
    :return: None
    :author: Jake Evenson
    """
    response = send_msg("MAIL FROM: <" + message_info["From"] + ">", tcp_socket)
    check_status_code(response, b'250')

    response = send_msg("RCPT TO: <" + message_info["To"] + ">", tcp_socket)
    check_status_code(response, b'250')


def send_message(message_info, message_text, tcp_socket):
    """
    Helper method for formatting and sending a message over a TLS connection
    :param message_info: meta data about the message being sent
    :param message_text: the body of the message to be sent
    :param tcp_socket: the socket over which to communicate to the server 
    :return: None
    :author: Jake Evenson
    """
    attachment = get_attachment(ATTACH)

    response = send_msg("DATA", tcp_socket)
    check_status_code(response, b'354')

    # Send message headers
    send_headers(message_info, tcp_socket)

    # Send body
    send_body(message_text, tcp_socket)

    # Send attachment
    attach_file(attachment, tcp_socket)

    # End the message
    end_message(tcp_socket)


def send_headers(message_info, tcp_socket):
    """
    Helper method for sending required headers for a message
    j
    :param message_info:
    :param tcp_socket: the socket over which to communicate to the server
    :return: None
    :author: Jake Evenson
    """
    tcp_socket.sendall(b'SUBJECT: ' + message_info["Subject"].encode("utf-8") + b'\r\n')
    tcp_socket.sendall(b'FROM: ' + message_info["From"].encode("utf-8") + b'\r\n')
    tcp_socket.sendall(b'TO: ' + message_info["To"].encode("utf-8") + b'\r\n')
    tcp_socket.sendall(b'DATE: ' + message_info["Date"].encode("utf-8") + b'\r\n')
    tcp_socket.sendall(b'MIME-Version: 1.0' + b'\r\n')
    tcp_socket.sendall(b'Content-Type: multipart/mixed; boundary=sep' + b'\r\n')


def send_body(message_text, tcp_socket):
    """
    Helper method for sending the body of the message
    :param message_text: the body of the message to be sent
    :param tcp_socket: the socket over which to communicate to the server
    :return: None
    :author: Jake Evenson
    """
    tcp_socket.sendall(b'--sep' + b'\r\n\r\n')
    tcp_socket.sendall(message_text.encode("utf-8") + b'\r\n')
    tcp_socket.sendall(b'--sep' + b'\r\n')


def attach_file(attachment, tcp_socket):
    """
    helper method for sending email attachments to a server using smtp
    :param attachment: The content of the attachment as a bytes object
    :param tcp_socket: the socket to send the attachment over
    :return: None
    :author: Jake Evenson
    """
    tcp_socket.sendall(b'Content--Type: application/octet-stream; name=\\"image.png\\"' + b'\r\n')
    tcp_socket.sendall(b'Content-Disposition: attachment; filename=\\"image.png\\"' + b'\r\n')
    tcp_socket.sendall(b'Content-Transfer-Encoding: base64' + b'\r\n')
    tcp_socket.sendall(b'\r\n' + b'\r\n')
    tcp_socket.sendall(base64.b64encode(attachment) + b'\r\n')
    tcp_socket.sendall(b'--sep--' + b'\r\n')
    tcp_socket.sendall(b'.' + b'\r\n' + b'QUIT' + b'\r\n')


def end_message(tcp_socket):
    """
    Helper method for ending a message
    :param tcp_socket: the socket over which to communicate to the server
    :return: None
    :author: Jake Evenson
    """
    response = send_msg(".", tcp_socket)
    check_status_code(response, b'250')


def send_msg(msg, tcp_socket):
    """
    Helper method for formatting a string to send over the tcp socket
    :param msg: the message as a sting to send
    :param tcp_socket: the socket over which to communicate to the server
    :return: the response from the server
    :rtype: If the response is one line: String
            Else: list of strings
    :author: Stuart Enters
    """
    print(msg)
    msg = str(msg) + "\r\n"
    tcp_socket.sendall(msg.encode())
    response = read_lines(tcp_socket)
    if len(response) == 1:
        return response[0]
    else:
        return response


def read_lines(tcp_socket):
    """
    Helper method for reading n lines from the tcp socket, where n is unknown
    :param tcp_socket: the socket over which to communicate to the server
    :return: a list of strings containing the lines read from the socket
    :author: Stuart Enters
    """
    response = list()
    response.append(read(tcp_socket))
    while b'-' in response[len(response) - 1]:
        response.append(read(tcp_socket))
    return response


def read(tcp_socket):
    """
    Helper method for reading a single line from a tcp socket
    :param tcp_socket: the socket over which to communicate to the server
    :return: the line read from the socket
    :author: Stuart Enters
    """
    msg = b''
    while not msg.endswith(b'\r\n'):
        msg += tcp_socket.recv(1)
    msg = msg.split(b'\r\n')[0]
    print(msg)
    return msg


def check_status_code(response, status_code):
    """
    Helper method for ensuring the expected status code was received from the SMTP server
    :param response: The response issued from the server
    :param status_code: The expected status code for the server to send
    :return: None
    """
    if not response.split(b' ')[0] == status_code:
        print(">>>Error with server interaction: \r\n" + response.decode("ASCII") + "\r\n")
        raise ValueError


def get_attachment(file_path):
    """
    Method for getting a requested file to send as an attachment
    :param file_path: the file path of the attachment to be sent
    :return: the contents of the attachment as a bytes object
    :author: Stuart Enters
    """
    # Check if file
    if os.path.isfile(file_path):
        file = open(file_path, "rb")

        # Get the content of the file
        msg = file.read(1)
        message = b''
        while msg != b'':
            message += msg
            msg = file.read(1)
        return message
    else:
        return get_attachment("./sample.png")


# ** Do not modify code below this line. **

# Utility functions
# You may use these functions to simplify your code.


def get_formatted_date():
    """Get the current date and time, in a format suitable for an email date header.

    The constant TIMEZONE_NAME should be one of the standard pytz timezone names.
    If you really want to see them all, call the print_all_timezones function.

    tzlocal suggested by http://stackoverflow.com/a/3168394/1048186

    See RFC 5322 for details about what the timezone should be
    https://tools.ietf.org/html/rfc5322

    :return: Formatted current date/time value, as a string.
    """
    zone = tzlocal.get_localzone()
    print("zone =", zone)
    timestamp = datetime.datetime.now(zone)
    timestring = timestamp.strftime('%a, %d %b %Y %H:%M:%S %z')  # Sun, 06 Nov 1994 08:49:37 +0000
    return timestring


def print_all_timezones():
    """ Print all pytz timezone strings. """
    for tz in pytz.all_timezones:
        print(tz)


# You probably won't need the following methods, unless you decide to
# try to handle email attachments or send multi-part messages.
# These advanced capabilities are not required for the lab assignment.


def get_mime_type(file_path):
    """Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: String containing path to (resource) file, such as './abc.jpg'
    :return: If successful in guessing the MIME type, a string representing the content
             type, such as 'image/jpeg'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """Try to get the size of a file (resource) in bytes, given its path

    :param file_path: String containing path to (resource) file, such as './abc.html'

    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()
